package rocks.runur.ih2

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.auth0.android.Auth0
import com.auth0.android.authentication.AuthenticationAPIClient
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.callback.Callback
import com.auth0.android.management.ManagementException
import com.auth0.android.management.UsersAPIClient
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import com.auth0.android.result.UserProfile
import com.google.android.material.snackbar.Snackbar
import rocks.runur.ih2.databinding.ActivityMainBinding
import rocks.runur.ih2.mqtt.MqttClient
import com.android.volley.toolbox.StringRequest
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.Volley

/**
 * Auth0 part supplied by this tutorial in two posts:
 * Part1: https://auth0.com/blog/get-started-with-android-authentication-using-kotlin-part-1/
 * Part2: https://auth0.com/blog/get-started-with-android-authentication-using-kotlin-part-2/
 */

class MainActivity : AppCompatActivity() {

	private val _loggedIn = MutableLiveData<Boolean>()
	val loggedIn: LiveData<Boolean>
		get() = _loggedIn

	private val _role = MutableLiveData<String>()
	val role: LiveData<String>
		get() = _role

	private lateinit var mqttClient: MqttClient

	private lateinit var binding: ActivityMainBinding

	// Represents the account to connect to auth0; this is not to be confused with the users account.
	private lateinit var account: Auth0
	private var cachedCredentials: Credentials? = null
	private var cachedUserProfile: UserProfile? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		mqttClient = MqttClient(this)

		// Setting the account for the ID service
		account = Auth0(
			getString(R.string.com_auth0_client_id),
			getString(R.string.com_auth0_domain)
		)
		binding = ActivityMainBinding.inflate(layoutInflater)
		setContentView(binding.root)
		binding.buttonLoginout.setOnClickListener{loginOut()}
		binding.buttonServo.setOnClickListener{servo()}
		binding.graphButton.setOnClickListener { getAppMetadata() }

		updateUI()

		getRequest()
	}

	private fun loginOut(){
		if (loggedIn.value == true){
			logout()
		}else{
			login()
		}
	}

	/**
	 * login() uses the Auth0 SDK’s WebAuthProvider class, which gives the app the ability to use Auth0’s authentication service
	 */
	private fun login(){
		WebAuthProvider
			/**
			 * initiates the login process and specifies the Auth0 account used by the application.
			 */
			.login(account)
			/**
			 * specifies the scheme to use for the URL that Auth0 redirects to after a successful login.
			 * For web apps, the scheme is http or https. This value is arbitrary for native mobile apps, so we use app to make
			 * it clear to other developers and other people who may use the Auth0 settings for this app that the redirect is not to a web page.
			 */
			.withScheme(getString(R.string.com_auth0_scheme))
			/**
			 * specifies which sets of user data the app is authorized to use if the user logs in successfully.
			 * The OpenID Connect and OAuth frameworks, on which Auth0’s authentication and authorization are based, use the term scope
			 * to represent the authorization to access user’s data and resources. The method takes a space-delimited string as its
			 * argument, where each “word” in the string specifies a different scope. The string used in this app contains these scopes:
			 * - openid: Indicates that application that uses OpenID Connect for authentication. This is the only required scope; all other scopes are optional.
			 * - profile: Authorizes the application to access basic user profile information, including first name, surname, nickname, their photo or avatar, and so on.
			 * - email: Authorizes the application to access the user’s email address.
			 * - read: current_user: Authorizes the application with read-only access to the current_user claim.
			 * - update: current_user_metadata: Authorizes the application with read and write access to the current_user_metadata claim. This scope allows us to get and set the country value in the user’s metadata.
			 */
			.withScope(getString(R.string.login_scopes))
			/**
			 * specifies the URL that the app will use to connect to Auth0’s login service.
			 * This URL is constructed using the domain of the Auth0 tenant used by the app and the endpoint for the Auth0 authentication API.
			 */
			.withAudience(getString(R.string.login_audience, getString(R.string.com_auth0_domain)))
			/**
			 * takes the WebAuthProvider object constructed by all the previous methods in the chain and opens the browser window to display the login page.
			 * It takes two parameters: a context (a reference to the Activity that’s initiating the browser window) and an anonymous object with two callback methods:
			 * - onFailure(): Defines what should happen if the user returns from the browser login screen without successfully logging in.
			 * 				  This typically happens when the user closes the browser login screen or taps the “back” button while on that screen.
			 * 				  The app displays a SnackBar that notifies the user that login failed, followed by an error code.
			 * - onSuccess(): Defines what should happen if the user returns from the browser login screen after successfully logging in.
			 * 				  The app processes the successful response, displays a SnackBar notifying the user that login was successful, and updates the UI to its “logged in” state.
			 */
			.start(this, object : Callback<Credentials, AuthenticationException>{
				override fun onFailure(error: AuthenticationException) {
					showSnackBar(getString(R.string.login_failure_message, error.getCode()))
				}

				override fun onSuccess(result: Credentials) {
					_loggedIn.value = true
					cachedCredentials = result
					showSnackBar(getString(R.string.login_success_message, result.accessToken))
					updateUI()
					showUserProfile()

					Log.i("MainActivity - login()", loggedIn.value.toString())
					Log.i("MainActivity - login()", result.accessToken)
					Log.i("MainActivity - login()", result.idToken)
					Log.i("MainActivity - login()", result.idToken.plus("scope: app://rocks.runur.ih2/role"))

				}
			})
	}

	private fun logout() {
		WebAuthProvider
			/**
			 * logout() initiates the logout process and specifies the Auth0 account used by the application, which should be the same account as the one used to log in.
			 */
			.logout(account)
			/**
			 * specifies the scheme to use for the URL that Auth0 redirects to after successful logout. This should be the same scheme as the one used to log in.
			 */
			.withScheme(getString(R.string.com_auth0_scheme))
			/**
			 * takes the WebAuthProvider object constructed by all the previous methods in the chain to log the user out.
			 * It takes two parameters: a context (a reference to the Activity that’s initiating the logout process) and an anonymous object with two callback methods:
			 * - onFailure(): Defines what should happen when the logout process fails. This rarely happens and usually indicates a network or server issue.
			 * 				  In this example, the app updates the UI (which remains in the “logged in” state) and displays a SnackBar that notifies the user
			 * 				  that logout failed, followed by an error code.
			 * - onSuccess(): Defines what should happen when the logout process succeeds. In this example, the app destroys its local copies of the user’s
			 * 				  credentials and profile and updates the UI to its “logged out” state.
			 */
			.start(this, object : Callback<Void?, AuthenticationException>{
				override fun onFailure(exception: AuthenticationException) {
					updateUI()
					showSnackBar(getString(R.string.general_failure_with_exception_code,
						exception.getCode()))
				}

				override fun onSuccess(result: Void?) {
					_loggedIn.value = false
					cachedCredentials = null
					cachedUserProfile = null
					_role.value = null
					binding.humidity.visibility = View.GONE
					binding.temperature.visibility = View.GONE
					updateUI()
					Log.i("MainActivity - logout()", loggedIn.value.toString())
				}
			})
	}

	private fun showUserProfile() {
		// Guard against showing the profile when no user is logged in
		if (cachedCredentials == null) {
			return
		}

		val client = AuthenticationAPIClient(account)
		client
			.userInfo(cachedCredentials!!.accessToken!!)
			.start(object : Callback<UserProfile, AuthenticationException> {

				override fun onFailure(exception: AuthenticationException) {
					showSnackBar(getString(R.string.general_failure_with_exception_code,
						exception.getCode()))
				}

				override fun onSuccess(profile: UserProfile) {
					cachedUserProfile = profile
					Log.i("MainActivity", profile.toString())
					updateUI()
				}

			})
	}

	private fun updateUI() {
		// Check if we're logged in
		if (loggedIn.value == true){
			binding.buttonLoginout.text = getString(R.string.logout)
			binding.greeting.visibility = View.VISIBLE
			binding.graphLayout.visibility = View.VISIBLE
			getGraphs()
		}else{
			binding.buttonLoginout.text = getString(R.string.login)
			binding.greeting.visibility = View.GONE
			binding.graphLayout.visibility = View.GONE
		}

		getGraphs()

		val userName = cachedUserProfile?.name ?: ""
		binding.greeting.text = getString(R.string.greeting, userName)
	}

	private fun showSnackBar(text: String) {
		Snackbar
			.make(
				binding.root,
				text,
				Snackbar.LENGTH_LONG
			).show()
	}

	private fun getAppMetadata() {
		// Guard against getting the metadata when no user is logged in
		if (cachedCredentials == null) {
			return
		}

		val usersClient = UsersAPIClient(account, cachedCredentials!!.accessToken!!)
		val test = UsersAPIClient(account, cachedCredentials!!.idToken!!)
		Log.i("MainActivity", test.getProfile("1").toString())

		usersClient
			.getProfile(cachedUserProfile!!.getId()!!)
			.start(object : Callback<UserProfile, ManagementException> {

				override fun onFailure(error: ManagementException) {
					showSnackBar(getString(R.string.general_failure_with_exception_code,
						error.getCode()))
				}

				override fun onSuccess(result: UserProfile) {
					cachedUserProfile = result
					_role.value = result.getAppMetadata()["role"] as String?
					getRequest()
					updateUI()
				}
			})
	}

	@SuppressLint("SetJavaScriptEnabled")
	private fun getGraphs(){
		when (role.value) {
			"rw" -> {
				// TODO: Get this to use retrofit
				binding.temperature.settings.javaScriptEnabled = true
				binding.temperature.settings.domStorageEnabled = true
				binding.temperature.settings.defaultTextEncodingName = "utf-8"
				// TODO: Find a way to make these string resources
				binding.temperature.loadUrl("https://runur.rocks/api/temperature.php")
				binding.temperature.webViewClient = WebViewClient()

				binding.humidity.settings.javaScriptEnabled = true
				binding.humidity.settings.domStorageEnabled = true
				binding.humidity.settings.defaultTextEncodingName = "utf-8"
				// TODO: Find a way to make these string resources
				binding.humidity.loadUrl("https://runur.rocks/api/humidity.php")
				binding.humidity.webViewClient = WebViewClient()
			}
			"r" -> {
				binding.temperature.visibility = View.GONE
				binding.humidity.visibility = View.GONE
				binding.graphButton.isEnabled = false
				Toast.makeText(this,"You're not allowed to see the graphs", Toast.LENGTH_LONG).show()
			}
			else -> {
				binding.graphButton.isEnabled = true
			}
		}
	}

	private fun servo() {

		val queue = Volley.newRequestQueue(this)
		val url: String = "https://api.thingspeak.com/update?api_key=XFBG1FSHROY1ZIUE&field3=1"

		val stringReq = StringRequest(Request.Method.GET, url,
			{ response ->
				val strResp = response.toString()
				Log.d("API", strResp)
			},
			{Log.d("API", "that didn't work") })
		queue.add(stringReq)
	}

	fun getRequest() {

		val stringRequest = StringRequest(

			Request.Method.GET,
			"https://api.thingspeak.com/channels/1719414/fields/1.json?api_key=6ZTUEP198XYLQWF7&results=2",

			{ responseString ->
				binding.reading.text = responseString.toString()
				Log.i("MainActivity", responseString)
			},
			{ volleyError ->
				val errorMessage = volleyError.message
				binding.reading.text = errorMessage
			}
		)

		Volley.newRequestQueue(this).add(stringRequest)

	}

}